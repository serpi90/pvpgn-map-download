const isWc3MapsLink =
  /^https:\/\/(?:www\.)?wc3maps\.com\/map\/(?<id>\d+)\/.*$/u;

export const WC3MapsDownload = {
  appliesTo(url) {
    return Boolean(isWc3MapsLink.exec(url));
  },
  example: "https://wc3maps.com/map/459/Bleach_VS_Onepiece_13.0_",
  async resolve(url) {
    const {
      groups: { id },
    } = isWc3MapsLink.exec(url);
    const response = await fetch(`https://www.wc3maps.com/api/download/${id}`, {
      method: "GET",
      headers: {
        Accept: "application/octet-stream",
        Referer: url,
      },
    });
    const fileName = response.headers.get("x-bz-file-name").split("/").pop();
    return {
      fileName,
      downloadStream: response.body,
    };
  },
};
