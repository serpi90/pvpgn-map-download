import { EpicWarDownload } from "./EpicWarDownload.js";
import { HiveWorkshopDownload } from "./HiveWorkshopDownload.js";
import { WC3MapsDownload } from "./WC3MapsDownload.js";

export const downloadTypes = [
  EpicWarDownload,
  HiveWorkshopDownload,
  WC3MapsDownload,
];
