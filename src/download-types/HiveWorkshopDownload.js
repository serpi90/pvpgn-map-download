import jQuery from "jquery";
import { JSDOM } from "jsdom";

const isHiveWorkshopLink =
  /^https:\/\/www\.hiveworkshop\.com\/threads\/[^/]*\.\d+\/?$/u;

export const HiveWorkshopDownload = {
  appliesTo(url) {
    return Boolean(isHiveWorkshopLink.exec(url));
  },
  example: "https://www.hiveworkshop.com/threads/battlestadium-don-2-0.119118/",
  async resolve(url) {
    let response = await fetch(url);
    const html = await response.text();
    const dom = new JSDOM(html);
    const $ = jQuery(dom.window);
    const downloadURL = `https://www.hiveworkshop.com${$("a.download").attr("href")}`;

    response = await fetch(downloadURL, { redirect: "manual" });
    const mapURL = response.headers.get("location");

    const fileName = decodeURI(mapURL.split("/").pop());
    response = await fetch(mapURL, {
      method: "GET",
      headers: { Accept: "application/octet-stream" },
    });
    return {
      fileName,
      downloadStream: response.body,
    };
  },
};
