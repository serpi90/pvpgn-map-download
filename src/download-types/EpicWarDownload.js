import jQuery from "jquery";
import { JSDOM } from "jsdom";

export const EpicWarDownload = {
  appliesTo(url) {
    const isEpicwarLink = /^https:\/\/www\.epicwar\.com\/maps\/(?<id>\d+)\/$/u;
    return Boolean(isEpicwarLink.exec(url));
  },
  example: "https://www.epicwar.com/maps/22972/",
  async resolve(url) {
    let response = await fetch(url);
    const html = await response.text();
    const dom = new JSDOM(html);
    const $ = jQuery(dom.window);
    const mapURL = `https://www.epicwar.com${$("td.listentry:nth-child(2) > a:nth-child(3)").attr("href")}`;
    const fileName = decodeURI(mapURL.split("/").pop());
    response = await fetch(mapURL, {
      method: "GET",
      headers: { Accept: "application/octet-stream" },
    });
    return {
      fileName,
      downloadStream: response.body,
    };
  },
};
