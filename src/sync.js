/* eslint-disable no-console */
import { REST, Routes } from "discord.js";
import dotenv from "dotenv";
import { slashCommands } from "./slash-commands/index.js";

dotenv.config();
const applicationId = process.env.DISCORD_APP_ID;
const token = process.env.DISCORD_AUTH_TOKEN;
const guildId = process.env.DISCORD_GUILD_ID;

const rest = new REST().setToken(token);
const body = slashCommands.map((command) => command.data.toJSON());
const route =
  process.env.NODE_ENV === "dev"
    ? Routes.applicationGuildCommands(applicationId, guildId)
    : Routes.applicationCommands(applicationId);

console.log(`Started refreshing ${body.length} application (/) commands.`);
const data = await rest.put(route, { body });
console.log(`Successfully reloaded ${data.length} application (/) commands.`);
