import { addMapByUrl } from "./add-map-by-url.js";
import { uploadMap } from "./upload-map.js";

export const slashCommands = [addMapByUrl, uploadMap];
