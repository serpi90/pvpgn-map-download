import { SlashCommandBuilder } from "discord.js";
import { downloadTypes } from "../download-types/index.js";
import { download } from "./util/download.js";

const invalidURL = `Invalid URL, valid examples are:\n
\`\`\`
${downloadTypes.map((dt) => `${dt.example}`).join("\n")}
\`\`\``;

export const addMapByUrl = {
  data: new SlashCommandBuilder()
    .setName("add-map-by-url")
    .setDescription("Adds a Wacraft III to the bot")
    .addStringOption((option) =>
      option
        .setName("url")
        .setDescription("URL to the map website")
        .setRequired(true),
    ),
  async execute(interaction) {
    const url = interaction.options.getString("url");
    const downloadType = downloadTypes.find((e) => e.appliesTo(url));
    if (!downloadType) {
      await interaction.reply({ content: invalidURL, ephemeral: true });
      return;
    }

    await interaction.deferReply({ ephemeral: true });
    const { fileName, downloadStream } = await downloadType.resolve(url);

    await download(fileName, downloadStream, interaction);
  },
};
