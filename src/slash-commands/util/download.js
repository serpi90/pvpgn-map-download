import { createWriteStream, existsSync } from "fs";
import { Readable } from "stream";
import { finished } from "stream/promises";

export async function download(mapFileName, downloadStream, interaction) {
  if (existsSync(`maps/${mapFileName}`)) {
    downloadStream.cancel();
    await interaction.followUp(`${mapFileName} already exists.`);
    return;
  }

  const fileStream = createWriteStream(`maps/${mapFileName}`);
  Readable.fromWeb(downloadStream).pipe(fileStream);
  await finished(fileStream);

  await interaction.followUp(`Downloaded: ${mapFileName}.`);
}
