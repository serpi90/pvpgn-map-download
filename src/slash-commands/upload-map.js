import { parse } from "content-disposition";
import { SlashCommandBuilder } from "discord.js";
import { download } from "./util/download.js";
import { extname } from "node:path";

export const uploadMap = {
  data: new SlashCommandBuilder()
    .setName("upload-map")
    .setDescription("Adds a Wacraft III to the bot")
    .addAttachmentOption((option) =>
      option
        .setName("map")
        .setDescription("w3x file for the map")
        .setRequired(true),
    ),
  async execute(interaction) {
    const attachment = interaction.options.getAttachment("map");
    if (![".w3x", ".w3m"].includes(extname(attachment.name))) {
      await interaction.reply({
        content: `Invalid file type: ${attachment.name} is not a Warcraft III map.`,
        ephemeral: true,
      });
      return;
    }

    await interaction.deferReply({ ephemeral: true });
    const response = await fetch(attachment.url, {
      method: "GET",
      headers: { Accept: "application/octet-stream" },
    });
    const fileName = parse(response.headers.get("content-disposition"))
      .parameters.filename;
    const downloadStream = response.body;

    await download(fileName, downloadStream, interaction);
  },
};
