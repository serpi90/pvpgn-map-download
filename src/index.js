/* eslint-disable no-console */
import { Client, Events } from "discord.js";
import dotenv from "dotenv";
import { slashCommands } from "./slash-commands/index.js";

dotenv.config();
const token = process.env.DISCORD_AUTH_TOKEN;

const client = new Client({ intents: [] });

client.once(Events.ClientReady, (readyClient) => {
  console.log(`Ready! Logged in as ${readyClient.user.tag}`);
});
client.on(Events.InteractionCreate, async (interaction) => {
  if (!interaction.isChatInputCommand()) return;
  const command = slashCommands.find(
    (c) => c.data.name === interaction.commandName,
  );

  if (!command) {
    console.error(`No command matching ${interaction.commandName} was found.`);
    return;
  }

  try {
    await command.execute(interaction);
  } catch (error) {
    console.error(error);
    const content = "There was an error while executing this command!";
    if (interaction.replied || interaction.deferred) {
      await interaction.followUp(content);
    } else {
      await interaction.reply({ content, ephemeral: true });
    }
  }
});

client.login(token);
