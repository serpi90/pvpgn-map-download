FROM node:20-alpine

WORKDIR /app

COPY package.json package-lock.json /app/
RUN npm ci --omit=dev

COPY src/ /app/src/

VOLUME /app/maps

STOPSIGNAL SIGTERM

CMD [ "node", "src/index.js" ]
