# PvPGN map downloader

Bot that listens on a PvPGN Chat channel, and downloads the map links posted there.

Links can be from the following websites (with the format of the example).

- epicwar, example: <https://www.epicwar.com/maps/154390/>
- hiveworkshop, eample: <https://www.hiveworkshop.com/threads/war-quest-world.328750/>
- wc3maps, example: <https://www.wc3maps.com/map/168378/Custom_Hero_Legend_v3.9>

## Setup

Create a [discord bot](https://discord.com/developers/applications)

Then on the oauth2 section, check `applications.commands` and copy that link, it should be look like: `https://discord.com/oauth2/authorize?client_id=xxx&scope=applications.commands` (with a proper id).

Use that link to add the discord bot to your server.

You'll need:

- the bot's id from the `General Information` tab, search for `Application ID`
- the bot's authorization token from the `Bot` tab, search for `Token`

Create a docker-compose.yml file with the following.

```yaml
services:
  map-downloader:
    build: 'https://gitlab.com/serpi90/pvpgn-map-download.git'
    environment:
      DISCORD_APP_ID: '<your-application-id>'
      DISCORD_AUTH_TOKEN: '<your-bot-auth-token>'
    restart: unless-stopped
    volumes:
      - <your-maps-path>:/app/maps
```

replace the values in `<>` with the appropiate values, and then run `docker compose up` on the folder where the yaml file is.
